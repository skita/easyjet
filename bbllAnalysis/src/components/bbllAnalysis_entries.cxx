#include "../BaselineVarsbbllAlg.h"
#include "../MMCDecoratorAlg.h"
#include "../HHbbllSelectorAlg.h"
#include "../SelectionFlagsbbllAlg.h"

using namespace HHBBLL;

DECLARE_COMPONENT(BaselineVarsbbllAlg)
DECLARE_COMPONENT(MMCDecoratorAlg)
DECLARE_COMPONENT(HHbbllSelectorAlg)
DECLARE_COMPONENT(SelectionFlagsbbllAlg)

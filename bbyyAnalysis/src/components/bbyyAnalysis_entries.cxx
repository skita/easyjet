#include "../BaselineVarsbbyyAlg.h"
#include "../SelectionFlagsbbyyAlg.h"
#include "../ResonantPNNbbyyAlg.h"

DECLARE_COMPONENT(HHBBYY::BaselineVarsbbyyAlg)
DECLARE_COMPONENT(HHBBYY::SelectionFlagsbbyyAlg)
DECLARE_COMPONENT(SHBBYY::ResonantPNNbbyyAlg)
